CREATE EXTENSION quaternion;

SELECT '(0.1, 0.2, 0.3, 0.4)'::quaternion;
SELECT ('(0.1, 0.2, 0.3, 0.4)'::quaternion)::text;

SELECT quaternion_equal('(0.1, 0.2, 0.3, 0.4)'::quaternion, '(0.1, 0.2, 0.3, 0.4)'::quaternion);
SELECT quaternion_equal('(0.1, 0.2, 0.3, 0.4)'::quaternion, '(0.5, 0.6, 0.7, 0.8)'::quaternion);

SELECT quaternion_not_equal('(0.1, 0.2, 0.3, 0.4)'::quaternion, '(0.1, 0.2, 0.3, 0.4)'::quaternion);
SELECT quaternion_not_equal('(0.1, 0.2, 0.3, 0.4)'::quaternion, '(0.5, 0.6, 0.7, 0.8)'::quaternion);

SELECT is_identity('(0.1, 0.2, 0.3, 0.4)'::quaternion);
SELECT is_identity('(0.0, 0.0, 0.0, 1.0)'::quaternion);

SELECT length('(0.1, 0.2, 0.3, 0.4)'::quaternion);
SELECT length_squared('(0.1, 0.2, 0.3, 0.4)'::quaternion);

SELECT normalize_q('(1.1, 2.2, 3.3, 4.4)'::quaternion);
SELECT inverse('(0.1, 0.2, 0.3, 0.4)'::quaternion);
SELECT conjugate('(0.1, 0.2, 0.3, 0.4)'::quaternion);


SELECT -'(0.1, 0.2, 0.3, 0.4)'::quaternion;
SELECT '(0.1, 0.2, 0.3, 0.4)'::quaternion + '(0.8, 0.7, 0.6, 0.5)'::quaternion;
SELECT '(0.1, 0.2, 0.3, 0.4)'::quaternion - '(0.8, 0.7, 0.6, 0.5)'::quaternion;

SELECT '(0.1, 0.2, 0.3, 0.4)'::quaternion * '(0.8, 0.7, 0.6, 0.5)'::quaternion;
SELECT '(0.1, 0.2, 0.3, 0.4)'::quaternion * 2.0;
SELECT 2 * '(0.1, 0.2, 0.3, 0.4)'::quaternion;

SELECT '(0.1, 0.2, 0.3, 0.4)'::quaternion / '(0.8, 0.7, 0.6, 0.5)'::quaternion;
SELECT '(0.1, 0.2, 0.3, 0.4)'::quaternion / 2.0;
SELECT 2 / '(0.1, 0.2, 0.3, 0.4)'::quaternion;

SELECT dot('(0.1, 0.2, 0.3, 0.4)'::quaternion, '(0.8, 0.7, 0.6, 0.5)'::quaternion);
SELECT concatenate('(0.1, 0.2, 0.3, 0.4)'::quaternion, '(0.8, 0.7, 0.6, 0.5)'::quaternion);
SELECT lerp('(0.1, 0.2, 0.3, 0.4)'::quaternion, '(0.8, 0.7, 0.6, 0.5)'::quaternion, 0.1);
SELECT slerp('(0.1, 0.2, 0.3, 0.4)'::quaternion, '(0.8, 0.7, 0.6, 0.5)'::quaternion, 0.1);

SELECT identity_q();
SELECT from_ypr(0, 0, 0);
SELECT from_ypr(1.571, 0.0, 3.142);

DROP EXTENSION quaternion;
