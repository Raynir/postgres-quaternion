-- Функции
CREATE OR REPLACE FUNCTION negate (q0 quaternion)
RETURNS quaternion AS
'quaternion', 'quaternion_negate'
LANGUAGE C IMMUTABLE STRICT;

CREATE OR REPLACE FUNCTION quaternion_add (q0 quaternion, q1 quaternion)
RETURNS quaternion AS
'quaternion', 'quaternion_add'
LANGUAGE C IMMUTABLE STRICT;

CREATE OR REPLACE FUNCTION quaternion_sub (q0 quaternion, q1 quaternion)
RETURNS quaternion AS
'quaternion', 'quaternion_sub'
LANGUAGE C IMMUTABLE STRICT;

CREATE OR REPLACE FUNCTION quaternion_mul (q0 quaternion, q1 quaternion)
RETURNS quaternion AS
'quaternion', 'quaternion_mul'
LANGUAGE C IMMUTABLE STRICT;

CREATE OR REPLACE FUNCTION quaternion_mul_left (q0 quaternion, k double precision)
RETURNS quaternion AS
'quaternion', 'quaternion_mul_left'
LANGUAGE C IMMUTABLE STRICT;

CREATE OR REPLACE FUNCTION quaternion_mul_right (k double precision, q0 quaternion)
RETURNS quaternion AS
'quaternion', 'quaternion_mul_right'
LANGUAGE C IMMUTABLE STRICT;

CREATE OR REPLACE FUNCTION quaternion_div (q0 quaternion, q1 quaternion)
RETURNS quaternion AS
'quaternion', 'quaternion_div'
LANGUAGE C IMMUTABLE STRICT;

CREATE OR REPLACE FUNCTION quaternion_div_left (q0 quaternion, k double precision)
RETURNS quaternion AS
'quaternion', 'quaternion_div_left'
LANGUAGE C IMMUTABLE STRICT;

CREATE OR REPLACE FUNCTION quaternion_div_right (k double precision, q0 quaternion)
RETURNS quaternion AS
'quaternion', 'quaternion_div_right'
LANGUAGE C IMMUTABLE STRICT;


-- Операторы
-- Унарный минус
CREATE OPERATOR -
(
    rightarg = quaternion,
    procedure = negate
);

-- Сложение
CREATE OPERATOR +
(
    leftarg = quaternion,
    rightarg = quaternion,
    procedure = quaternion_add,
    commutator = +
);

-- Вычитание
CREATE OPERATOR -
(
    leftarg = quaternion,
    rightarg = quaternion,
    procedure = quaternion_sub
);

-- Умножение кватернионов
CREATE OPERATOR *
(
    leftarg = quaternion,
    rightarg = quaternion,
    procedure = quaternion_mul
);

-- Умножение кватерниона на скаляр
CREATE OPERATOR *
(
    leftarg = quaternion,
    rightarg = double precision,
    procedure = quaternion_mul_left
);

-- Умножение скаляра на кватернион
CREATE OPERATOR *
(
    leftarg = double precision,
    rightarg = quaternion,
    procedure = quaternion_mul_right
);

-- Деление кватернионов
CREATE OPERATOR /
(
    leftarg = quaternion,
    rightarg = quaternion,
    procedure = quaternion_div
);

-- Деление кватерниона на скаляр
CREATE OPERATOR /
(
    leftarg = quaternion,
    rightarg = double precision,
    procedure = quaternion_div_left
);

-- Деление скаляра на кватернион
CREATE OPERATOR /
(
    leftarg = double precision,
    rightarg = quaternion,
    procedure = quaternion_div_right
);

-- Проверка на равенство
CREATE OPERATOR =
(
    leftarg = quaternion,
    rightarg = quaternion,
    procedure = quaternion_equal
);

-- Проверка на неравенство
CREATE OPERATOR !=
(
    leftarg = quaternion,
    rightarg = quaternion,
    procedure = quaternion_not_equal
);
