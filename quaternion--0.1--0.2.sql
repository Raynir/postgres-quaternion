CREATE OR REPLACE FUNCTION quaternion_equal (q0 quaternion, q1 quaternion)
RETURNS boolean AS
'quaternion', 'quaternion_equal'
LANGUAGE C IMMUTABLE STRICT;

CREATE OR REPLACE FUNCTION quaternion_not_equal (q0 quaternion, q1 quaternion)
RETURNS boolean AS
'quaternion', 'quaternion_not_equal'
LANGUAGE C IMMUTABLE STRICT;

CREATE OR REPLACE FUNCTION length (q0 quaternion)
RETURNS double precision AS
'quaternion', 'quaternion_length'
LANGUAGE C IMMUTABLE STRICT;

CREATE OR REPLACE FUNCTION length_squared (q0 quaternion)
RETURNS double precision AS
'quaternion', 'quaternion_length_squared'
LANGUAGE C IMMUTABLE STRICT;

CREATE OR REPLACE FUNCTION normalize_q (q0 quaternion)
RETURNS quaternion AS
'quaternion', 'quaternion_normalize'
LANGUAGE C IMMUTABLE STRICT;

CREATE OR REPLACE FUNCTION inverse (q0 quaternion)
RETURNS quaternion AS
'quaternion', 'quaternion_inverse'
LANGUAGE C IMMUTABLE STRICT;
