CREATE OR REPLACE FUNCTION is_identity (q0 quaternion)
RETURNS boolean AS
'quaternion', 'quaternion_is_identity'
LANGUAGE C IMMUTABLE STRICT;

CREATE OR REPLACE FUNCTION conjugate (q0 quaternion)
RETURNS quaternion AS
'quaternion', 'quaternion_conjugate'
LANGUAGE C IMMUTABLE STRICT;


CREATE OR REPLACE FUNCTION dot (q0 quaternion, q1 quaternion)
RETURNS double precision AS
'quaternion', 'quaternion_dot'
LANGUAGE C IMMUTABLE STRICT;

CREATE OR REPLACE FUNCTION concatenate (q0 quaternion, q1 quaternion)
RETURNS quaternion AS
'quaternion', 'quaternion_concatenate'
LANGUAGE C IMMUTABLE STRICT;

CREATE OR REPLACE FUNCTION lerp (q0 quaternion, q1 quaternion, amount double precision)
RETURNS quaternion AS
'quaternion', 'quaternion_lerp'
LANGUAGE C IMMUTABLE STRICT;

CREATE OR REPLACE FUNCTION slerp (q0 quaternion, q1 quaternion, amount double precision)
RETURNS quaternion AS
'quaternion', 'quaternion_slerp'
LANGUAGE C IMMUTABLE STRICT;


CREATE OR REPLACE FUNCTION identity_q ()
RETURNS quaternion AS
'quaternion', 'quaternion_identity'
LANGUAGE C IMMUTABLE STRICT;

CREATE OR REPLACE FUNCTION from_ypr (yaw double precision, pitch double precision, roll double precision)
RETURNS quaternion AS
'quaternion', 'quaternion_from_yaw_pitch_roll'
LANGUAGE C IMMUTABLE STRICT;