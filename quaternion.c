#include <postgres.h>
#include <fmgr.h>
#include <libpq/pqformat.h>
#include <math.h>

#ifdef PG_MODULE_MAGIC
    PG_MODULE_MAGIC;
#endif

// Типы
typedef struct
{
    double x, y, z, w;
} quaternion;

// Объявления
PG_FUNCTION_INFO_V1(quaternion_in);
PG_FUNCTION_INFO_V1(quaternion_out);
PG_FUNCTION_INFO_V1(quaternion_recv);
PG_FUNCTION_INFO_V1(quaternion_send);

PG_FUNCTION_INFO_V1(quaternion_equal);
PG_FUNCTION_INFO_V1(quaternion_not_equal);
PG_FUNCTION_INFO_V1(quaternion_is_identity);

PG_FUNCTION_INFO_V1(quaternion_length);
PG_FUNCTION_INFO_V1(quaternion_length_squared);
PG_FUNCTION_INFO_V1(quaternion_normalize);
PG_FUNCTION_INFO_V1(quaternion_inverse);
PG_FUNCTION_INFO_V1(quaternion_conjugate);

PG_FUNCTION_INFO_V1(quaternion_negate);
PG_FUNCTION_INFO_V1(quaternion_add);
PG_FUNCTION_INFO_V1(quaternion_sub);
PG_FUNCTION_INFO_V1(quaternion_mul);
PG_FUNCTION_INFO_V1(quaternion_mul_left);
PG_FUNCTION_INFO_V1(quaternion_mul_right);
PG_FUNCTION_INFO_V1(quaternion_div);
PG_FUNCTION_INFO_V1(quaternion_div_left);
PG_FUNCTION_INFO_V1(quaternion_div_right);
PG_FUNCTION_INFO_V1(quaternion_dot);
PG_FUNCTION_INFO_V1(quaternion_concatenate);
PG_FUNCTION_INFO_V1(quaternion_lerp);
PG_FUNCTION_INFO_V1(quaternion_slerp);

PG_FUNCTION_INFO_V1(quaternion_identity);
PG_FUNCTION_INFO_V1(quaternion_from_yaw_pitch_roll);

// Реализации
// Конвертация типа
// Конвертация строки в тип
Datum quaternion_in(PG_FUNCTION_ARGS)
{
    char *s = PG_GETARG_CSTRING(0);

    quaternion *q = (quaternion*)palloc(sizeof(quaternion));

    if (sscanf(s, "(%lf,%lf,%lf,%lf)", &(q->x), &(q->y), &(q->z), &(q->w)) != 4)
    {
        ereport(ERROR, (errcode(ERRCODE_INVALID_TEXT_REPRESENTATION), errmsg("Invalid input syntax for quaternion: \"%s\"", s)));
    }

    PG_RETURN_POINTER(q);
}

// Конвертация типа в строку
Datum quaternion_out(PG_FUNCTION_ARGS)
{
    quaternion *q = (quaternion*)PG_GETARG_POINTER(0);

    char *s = (char*)palloc(100);

    snprintf(s, 100, "(%lf,%lf,%lf,%lf)", q->x, q->y, q->z, q->w);

    PG_RETURN_CSTRING(s);
}

// Конвертация из бинарной формы в тип
Datum quaternion_recv(PG_FUNCTION_ARGS)
{
    StringInfo buffer = (StringInfo)PG_GETARG_POINTER(0);

    quaternion *q = (quaternion*)palloc(sizeof(quaternion));

    q->x = pq_getmsgfloat8(buffer);
    q->y = pq_getmsgfloat8(buffer);
    q->z = pq_getmsgfloat8(buffer);
    q->w = pq_getmsgfloat8(buffer);

    PG_RETURN_POINTER(q);
}

// Конвертация из типа в бинарную форму
Datum quaternion_send(PG_FUNCTION_ARGS)
{
    quaternion *q = (quaternion*)PG_GETARG_POINTER(0);

    StringInfoData buffer;

    pq_sendfloat8(&buffer, q->x);
    pq_sendfloat8(&buffer, q->y);
    pq_sendfloat8(&buffer, q->z);
    pq_sendfloat8(&buffer, q->w);

    PG_RETURN_BYTEA_P(pq_endtypsend(&buffer));
}

// Логические операции
// Проверка на равенство 
Datum quaternion_equal(PG_FUNCTION_ARGS)
{
    quaternion *q0 = (quaternion*)PG_GETARG_POINTER(0);
    quaternion *q1 = (quaternion*)PG_GETARG_POINTER(1);

    bool equal = true;

    equal &= q0->x == q1->x;
    equal &= q0->y == q1->y;
    equal &= q0->z == q1->z;
    equal &= q0->w == q1->w;

    PG_RETURN_BOOL(equal);
}

// Проверка на неравенство
Datum quaternion_not_equal(PG_FUNCTION_ARGS)
{
    quaternion *q0 = (quaternion*)PG_GETARG_POINTER(0);
    quaternion *q1 = (quaternion*)PG_GETARG_POINTER(1);

    bool not_equal = false;

    not_equal |= q0->x != q1->x;
    not_equal |= q0->y != q1->y;
    not_equal |= q0->z != q1->z;
    not_equal |= q0->w != q1->w;

    PG_RETURN_BOOL(not_equal);
}

// Проверка того, является ли кватернион единичным
Datum quaternion_is_identity(PG_FUNCTION_ARGS)
{
    quaternion *q0 = (quaternion*)PG_GETARG_POINTER(0);

    bool is_identity = true;

    is_identity &= q0->x == 0.0;
    is_identity &= q0->y == 0.0;
    is_identity &= q0->z == 0.0;
    is_identity &= q0->w == 1.0;

    PG_RETURN_BOOL(is_identity);
}

// Операции над одним кватернионом
// Длина кватерниона
Datum quaternion_length(PG_FUNCTION_ARGS)
{
    quaternion *q = (quaternion*)PG_GETARG_POINTER(0);

    double len = sqrt(q->x * q->x + q->y * q->y + q->z * q->z + q->w * q->w);

    PG_RETURN_FLOAT8(len);
}

// Длина кватерниона в квадрате, более дешевая операция
Datum quaternion_length_squared(PG_FUNCTION_ARGS)
{
    quaternion *q = (quaternion*)PG_GETARG_POINTER(0);

    double len = q->x * q->x + q->y * q->y + q->z * q->z + q->w * q->w;

    PG_RETURN_FLOAT8(len);
}

// Нормализация кватерниона
Datum quaternion_normalize(PG_FUNCTION_ARGS)
{
    quaternion *q0 = (quaternion*)PG_GETARG_POINTER(0);
    quaternion *q = (quaternion*)palloc(sizeof(quaternion));

    double ls = q0->x * q0->x + q0->y * q0->y + q0->z * q0->z + q0->w * q0->w;
    double inv_norm = 1.0 / sqrt(ls);

    q->x = q0->x * inv_norm;
    q->y = q0->y * inv_norm;
    q->z = q0->z * inv_norm;
    q->w = q0->w * inv_norm;

    PG_RETURN_POINTER(q);
}

// Обратный кватернион
Datum quaternion_inverse(PG_FUNCTION_ARGS)
{
    quaternion *q0 = (quaternion*)PG_GETARG_POINTER(0);
    quaternion *q = (quaternion*)palloc(sizeof(quaternion));

    double ls = q0->x * q0->x + q0->y * q0->y + q0->z * q0->z + q0->w * q0->w;
    double inv_norm = 1.0 / ls;

    q->x = -q0->x * inv_norm;
    q->y = -q0->y * inv_norm;
    q->z = -q0->z * inv_norm;
    q->w = q0->w * inv_norm;

    PG_RETURN_POINTER(q);
}

// Сопряженный кватернион
Datum quaternion_conjugate(PG_FUNCTION_ARGS)
{
    quaternion *q0 = (quaternion*)PG_GETARG_POINTER(0);
    quaternion *q = (quaternion*)palloc(sizeof(quaternion));

    q->x = -q0->x;
    q->y = -q0->y;
    q->z = -q0->z;
    q->w = q0->w;

    PG_RETURN_POINTER(q);
}

// Функциий для операторов
// Унарный минус
Datum quaternion_negate(PG_FUNCTION_ARGS)
{
    quaternion *q0 = (quaternion*)PG_GETARG_POINTER(0);
    quaternion *q = (quaternion*)palloc(sizeof(quaternion));

    q->x = -q0->x;
    q->y = -q0->y;
    q->z = -q0->z;
    q->w = -q0->w;

    PG_RETURN_POINTER(q);
}

// Сумма
Datum quaternion_add(PG_FUNCTION_ARGS)
{
    quaternion *q0 = (quaternion*)PG_GETARG_POINTER(0);
    quaternion *q1 = (quaternion*)PG_GETARG_POINTER(1);
    quaternion *q = (quaternion*)palloc(sizeof(quaternion));

    q->x = q0->x + q1->x;
    q->y = q0->y + q1->y;
    q->z = q0->z + q1->z;
    q->w = q0->w + q1->w;

    PG_RETURN_POINTER(q);
}

// Разность
Datum quaternion_sub(PG_FUNCTION_ARGS)
{
    quaternion *q0 = (quaternion*)PG_GETARG_POINTER(0);
    quaternion *q1 = (quaternion*)PG_GETARG_POINTER(1);
    quaternion *q = (quaternion*)palloc(sizeof(quaternion));

    q->x = q0->x - q1->x;
    q->y = q0->y - q1->y;
    q->z = q0->z - q1->z;
    q->w = q0->w - q1->w;

    PG_RETURN_POINTER(q);
}

// Умножение кватернионов
Datum quaternion_mul(PG_FUNCTION_ARGS)
{
    quaternion *q0 = (quaternion*)PG_GETARG_POINTER(0);
    quaternion *q1 = (quaternion*)PG_GETARG_POINTER(1);
    quaternion *q = (quaternion*)palloc(sizeof(quaternion));

    double q0x = q0->x;
    double q0y = q0->y;
    double q0z = q0->z;
    double q0w = q0->w;

    double q1x = q1->x;
    double q1y = q1->y;
    double q1z = q1->z;
    double q1w = q1->w;

    double cx = q0y * q1z - q0z * q1y;
    double cy = q0z * q1x - q0x * q1z;
    double cz = q0x * q1y - q0y * q1x;

    double dot = q0x * q1x + q0y * q1y + q0z * q1z;

    q->x = q0x * q1w + q1x * q0w + cx;
    q->y = q0y * q1w + q1y * q0w + cy;
    q->z = q0z * q1w + q1z * q0w + cz;
    q->w = q0w * q1w - dot;

    PG_RETURN_POINTER(q);
}

// Умножение кватерниона на скаляр
Datum quaternion_mul_left(PG_FUNCTION_ARGS)
{
    quaternion *q0 = (quaternion*)PG_GETARG_POINTER(0);
    double k = PG_GETARG_FLOAT8(1);
    quaternion *q = (quaternion*)palloc(sizeof(quaternion));

    q->x = q0->x * k;
    q->y = q0->y * k;
    q->z = q0->z * k;
    q->w = q0->w * k;

    PG_RETURN_POINTER(q);
}

// Умножение скаляра на кватернион
Datum quaternion_mul_right(PG_FUNCTION_ARGS)
{
    double k = PG_GETARG_FLOAT8(0);
    quaternion *q0 = (quaternion*)PG_GETARG_POINTER(1);
    quaternion *q = (quaternion*)palloc(sizeof(quaternion));

    q->x = k * q0->x;
    q->y = k * q0->y;
    q->z = k * q0->z;
    q->w = k * q0->w;

    PG_RETURN_POINTER(q);
}

// Деление кватернионов
Datum quaternion_div(PG_FUNCTION_ARGS)
{
    quaternion *q0 = (quaternion*)PG_GETARG_POINTER(0);
    quaternion *q1 = (quaternion*)PG_GETARG_POINTER(1);
    quaternion *q = (quaternion*)palloc(sizeof(quaternion));

    double q0x = q0->x;
    double q0y = q0->y;
    double q0z = q0->z;
    double q0w = q0->w;

    double ls = q1->x * q1->x + q1->y * q1->y +
                q1->z * q1->z + q1->w * q1->w;
    double inv_norm = 1.0 / ls;

    double q1x = -q1->x * inv_norm;
    double q1y = -q1->y * inv_norm;
    double q1z = -q1->z * inv_norm;
    double q1w = q1->w * inv_norm;

    double cx = q0y * q1z - q0z * q1y;
    double cy = q0z * q1x - q0x * q1z;
    double cz = q0x * q1y - q0y * q1x;

    double dot = q0x * q1x + q0y * q1y + q0z * q1z;

    q->x = q0x * q1w + q1x * q0w + cx;
    q->y = q0y * q1w + q1y * q0w + cy;
    q->z = q0z * q1w + q1z * q0w + cz;
    q->w = q0w * q1w - dot;

    PG_RETURN_POINTER(q);
}

// Деление кватерниона на скаляр
Datum quaternion_div_left(PG_FUNCTION_ARGS)
{
    quaternion *q0 = (quaternion*)PG_GETARG_POINTER(0);
    double k = PG_GETARG_FLOAT8(1);
    quaternion *q = (quaternion*)palloc(sizeof(quaternion));

    double inv_norm = 1.0 / k;

    q->x = q0->x * inv_norm;
    q->y = q0->y * inv_norm;
    q->z = q0->z * inv_norm;
    q->w = q0->w * inv_norm;

    PG_RETURN_POINTER(q);
}

// Деление скаляра на кватернион
// Вообще, в такой операции смысла нет. А здесь
// https://www.winehq.org/pipermail/wine-cvs/2007-November/037925.html ,
// например, подобная операция отсутствует. Но пусть будет в виде
// умножения скаляра на инверсированный кватернион
Datum quaternion_div_right(PG_FUNCTION_ARGS)
{
    double k = PG_GETARG_FLOAT8(0);
    quaternion *q0 = (quaternion*)PG_GETARG_POINTER(1);
    quaternion *q = (quaternion*)palloc(sizeof(quaternion));

    double ls = q0->x * q0->x + q0->y * q0->y + q0->z * q0->z + q0->w * q0->w;
    double inv_norm = 1.0 / ls;

    // Инверсия кватерниона
    q->x = -q0->x * inv_norm;
    q->y = -q0->y * inv_norm;
    q->z = -q0->z * inv_norm;
    q->w = q0->w * inv_norm;

    // Умножение скаляра на кватернион
    q->x = k * q->x;
    q->y = k * q->y;
    q->z = k * q->z;
    q->w = k * q->w;

    PG_RETURN_POINTER(q);
}

// Скалярное произведение кватернионов
Datum quaternion_dot(PG_FUNCTION_ARGS)
{
    quaternion *q0 = (quaternion*)PG_GETARG_POINTER(0);
    quaternion *q1 = (quaternion*)PG_GETARG_POINTER(1);

    double dot = q0->x * q1->x +
                 q0->y * q1->y +
                 q0->z * q1->z +
                 q0->w * q1->w;

    PG_RETURN_FLOAT8(dot);
}

// Конкатенация двух кватернионов
Datum quaternion_concatenate(PG_FUNCTION_ARGS)
{
    quaternion *q0 = (quaternion*)PG_GETARG_POINTER(0);
    quaternion *q1 = (quaternion*)PG_GETARG_POINTER(1);
    quaternion *q = (quaternion*)palloc(sizeof(quaternion));
    
    double q0x = q1->x;
    double q0y = q1->y;
    double q0z = q1->z;
    double q0w = q1->w;

    double q1x = q0->x;
    double q1y = q0->y;
    double q1z = q0->z;
    double q1w = q0->w;

    double cx = q0y * q1z - q0z * q1y;
    double cy = q0z * q1x - q0x * q1z;
    double cz = q0x * q1y - q0y * q1x;

    double dot = q0x * q1x + q0y * q1y + q0z * q1z;

    q->x = q0x * q1w + q1x * q0w + cx;
    q->y = q0y * q1w + q1y * q0w + cy;
    q->z = q0z * q1w + q1z * q0w + cz;
    q->w = q0w * q1w - dot;

    PG_RETURN_POINTER(q);
}

// Линейная интерполяция между двумя кватернионами
Datum quaternion_lerp(PG_FUNCTION_ARGS)
{
    quaternion *q0 = (quaternion*)PG_GETARG_POINTER(0);
    quaternion *q1 = (quaternion*)PG_GETARG_POINTER(1);
    double amount = PG_GETARG_FLOAT8(2);

    double ls, inv_norm;
    double t = amount;
    double t1 = 1.0 - t;

    quaternion *q = (quaternion*)palloc(sizeof(quaternion));

    double dot = q0->x * q1->x +
                 q0->y * q1->y +
                 q0->z * q1->z +
                 q0->w * q1->w;

    if (dot >= 0.0)
    {
        q->x = t1 * q0->x + t * q1->x;
        q->y = t1 * q0->y + t * q1->y;
        q->z = t1 * q0->z + t * q1->z;
        q->w = t1 * q0->w + t * q1->w;
    }
    else
    {
        q->x = t1 * q0->x - t * q1->x;
        q->y = t1 * q0->y - t * q1->y;
        q->z = t1 * q0->z - t * q1->z;
        q->w = t1 * q0->w - t * q1->w;
    }

    ls = q->x * q->x + q->y * q->y + q->z * q->z + q->w * q->w;
    inv_norm = 1.0 / sqrt(ls);

    q->x *= inv_norm;
    q->y *= inv_norm;
    q->z *= inv_norm;
    q->w *= inv_norm;

    PG_RETURN_POINTER(q);
}

// Сферическая линейная интерполяция между двумя кватернионами
Datum quaternion_slerp(PG_FUNCTION_ARGS)
{
    quaternion *q0 = (quaternion*)PG_GETARG_POINTER(0);
    quaternion *q1 = (quaternion*)PG_GETARG_POINTER(1);
    double amount = PG_GETARG_FLOAT8(2);

    quaternion *q = (quaternion*)palloc(sizeof(quaternion));

    const double eps = 1e-6;
    double t = amount;
    double cos_omega = q0->x * q1->x + q0->y * q1->y +
                       q0->z * q1->z + q0->w * q1->w;
    double s1, s2;
    bool flip = false;

    if (cos_omega < 0.0)
    {
        flip = true;
        cos_omega = -cos_omega;
    }

    if (cos_omega > (1.0 - eps))
    {
        s1 = 1.0 - t;
        s2 = (flip) ? -t : t;
    }
    else
    {
        double omega = acos(cos_omega);
        double inv_sin_omega = 1.0 / sin(omega);

        s1 = sin((1.0 - t) * omega) * inv_sin_omega;
        s2 = (flip)
            ? -sin(t * omega) * inv_sin_omega
            : sin(t * omega) * inv_sin_omega;
    }

    q->x = s1 * q0->x + s2 * q1->x;
    q->y = s1 * q0->y + s2 * q1->y;
    q->z = s1 * q0->z + s2 * q1->z;
    q->w = s1 * q0->w + s2 * q1->w;

    PG_RETURN_POINTER(q);
}

// Создание кватернионов
// Единичный
Datum quaternion_identity(PG_FUNCTION_ARGS)
{
    quaternion *q = (quaternion*)palloc(sizeof(quaternion));

    q->x = 0.0;
    q->y = 0.0;
    q->z = 0.0;
    q->w = 1.0;

    PG_RETURN_POINTER(q);
}

// Из рыскания, тангажа и крена (в радианах)
Datum quaternion_from_yaw_pitch_roll(PG_FUNCTION_ARGS)
{
    double yaw = PG_GETARG_FLOAT8(0);
    double pitch = PG_GETARG_FLOAT8(1);
    double roll = PG_GETARG_FLOAT8(2);

    double sr, cr, sp, cp, sy, cy;
    double half_roll, half_pitch, half_yaw;
    quaternion *q = (quaternion*)palloc(sizeof(quaternion));

    half_roll = roll * 0.5;
    sr = sin(half_roll);
    cr = cos(half_roll);

    half_pitch = pitch * 0.5;
    sp = sin(half_pitch);
    cp = cos(half_pitch);

    half_yaw = yaw * 0.5;
    sy = sin(half_yaw);
    cy = cos(half_yaw);

    q->x = cy * sp * cr + sy * cp * sr;
    q->y = sy * cp * cr - cy * sp * sr;
    q->z = cy * cp * sr - sy * sp * cr;
    q->w = cy * cp * cr + sy * sp * sr;

    PG_RETURN_POINTER(q);
}