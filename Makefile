MODULES = quaternion
EXTENSION = quaternion
DATA = quaternion--0.1.sql quaternion--0.1--0.2.sql quaternion--0.2--1.0.sql \
	quaternion--1.0--1.1.sql
REGRESS = quaternion

PG_CONFIG = pg_config
PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)
