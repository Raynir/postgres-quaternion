CREATE TYPE quaternion;

CREATE OR REPLACE FUNCTION quaternion_in (s cstring)
RETURNS quaternion AS
'quaternion', 'quaternion_in'
LANGUAGE C IMMUTABLE STRICT;

CREATE OR REPLACE FUNCTION quaternion_out (q quaternion)
RETURNS cstring AS
'quaternion', 'quaternion_out'
LANGUAGE C IMMUTABLE STRICT;

CREATE OR REPLACE FUNCTION quaternion_recv (p internal)
RETURNS quaternion AS
'quaternion', 'quaternion_recv'
LANGUAGE C IMMUTABLE STRICT;

CREATE OR REPLACE FUNCTION quaternion_send (q quaternion)
RETURNS bytea AS
'quaternion', 'quaternion_send'
LANGUAGE C IMMUTABLE STRICT;

CREATE TYPE quaternion
(
    internallength = 32,
    input = quaternion_in,
    output = quaternion_out,
    receive = quaternion_recv,
    send = quaternion_send
);